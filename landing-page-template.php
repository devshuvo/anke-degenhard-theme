<?php
/**
 * Template Name: Landing Page
 *
 */

get_header();
?>
	<div id="primary" class="content-area col-12">
		<main id="main" class="site-main">
			<div class="row">
				<div class="col-md-3">
					<div class="landing-left">
						<a href="<?php the_field('current_viewing_room_button_link'); ?>" class="btn btn-primary current-view">CURRENT VIEWING ROOM</a>
						<a href="<?php the_field('select_past_viewing_rooms_button_link'); ?>" class="btn btn-primary past-view">SELECT PAST VIEWING ROOMS</a>
						<div class="short-content">
							<?php the_field('left_side_short_description'); ?>
						</div>
						<a href="<?php the_field('subscribe_button_link'); ?>" class="btn btn-primary subscribe-link">SUBSCRIBE</a>
					</div>
				</div>
				<div class="col-md-9">
					<div class="landing-right">
						<?php echo anke_landing_room(); ?>
					</div>
				</div>
			</div>
		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
