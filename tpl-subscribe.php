<?php
/**
 * Template Name: Subscribe Page
 *
 */

get_header();
?>
	<div id="primary" class="content-area col-12">
		<main id="main" class="site-main">
			<div class="container text-center">
				<div class="subscribe-page-wrap">
					<?php
					while ( have_posts() ) :
						the_post();

						the_content();

					endwhile; // End of the loop.
					?>
				</div>	
			</div>	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
