<?php
/**
 * Sandhas functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sandhas
 */

if ( ! function_exists( 'sandhas_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sandhas_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Sandhas, use a find and replace
		 * to change 'sandhas' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'anke', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'sandhas' ),
			'menu-2' => esc_html__( 'Shop Menu', 'sandhas' ),
		) );



		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'sandhas_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/*Image Sizes*/
		add_image_size( 'brand_slider', 225, 150, true );
		add_image_size( 'sd_offer_slider', 363, 211, true ); //363x211
	}
endif;
add_action( 'after_setup_theme', 'sandhas_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sandhas_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'sandhas_content_width', 640 );
}
add_action( 'after_setup_theme', 'sandhas_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sandhas_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sandhas' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sandhas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
}
add_action( 'widgets_init', 'sandhas_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function sandhas_scripts() {

	$version = wp_get_theme()->get( 'Version' );

	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
	wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.css');
	wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
	wp_enqueue_style('owl-theme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css');
	wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css');
	wp_enqueue_style('slicknav', get_template_directory_uri() . '/assets/css/slicknav.min.css');

	
	wp_enqueue_style( 'sandhas-style', get_stylesheet_uri(), '', $version );
	wp_enqueue_style('anke-fonts', get_template_directory_uri() . '/assets/css/webfonts.css');
	

	// Enqueue fonts
	//wp_enqueue_style( 'sandhas-google-fonts', sandhas_google_fonts_url(), array(), '1.0.0' );
	wp_enqueue_style('sandhas-google-fonts', get_template_directory_uri() . '/assets/css/google-fonts.css');

	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('slicknav', get_template_directory_uri() . '/assets/js/jquery.slicknav.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('wow', get_template_directory_uri() . '/assets/js/wow.min.js', array( 'jquery' ), '', true);
	
	wp_enqueue_script( 'sandhas-script', get_template_directory_uri() . '/assets/js/active.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sandhas_scripts' );

/**
 * TGM Activation
 */
require dirname( __FILE__ ) . '/inc/tgm/tgm-init.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * ACF Fields
 */
require get_template_directory() . '/inc/acf-export.php';


/**
 * Post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 *	ACF Options Page
 *
 **/
if( function_exists('acf_add_options_page') ) {
	$args = array(
		
		/* (string) The title displayed on the options page. Required. */
		'page_title' => 'Theme Options',
		
		/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
		'menu_title' => '',
		
		/* (string) The URL slug used to uniquely identify this options page. 
		Defaults to a url friendly version of menu_title */
		'menu_slug' => '',
		
		/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
		Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
		'capability' => 'edit_posts',
		
		/* (int|string) The position in the menu order this menu should appear. 
		WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
		Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
		Defaults to bottom of utility menu items */
		'position' => false,
		
		/* (string) The slug of another WP admin page. if set, this will become a child page. */
		'parent_slug' => 'themes.php',
		
		/* (string) The icon class for this menu. Defaults to default WordPress gear.
		Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
		'icon_url' => false,
		
		/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists). 
		If set to false, this parent page will appear alongside any child pages. Defaults to true */
		'redirect' => true,
		
		/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2'). 
		Defaults to 'options'. Added in v5.2.7 */
		'post_id' => 'options',
		
		/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up. 
		Defaults to false. Added in v5.2.8. */
		'autoload' => false,
		
		/* (string) The update button text. Added in v5.3.7. */
		'update_button'		=> __('Save', 'acf'),
		
		/* (string) The message shown above the form on submit. Added in v5.6.0. */
		'updated_message'	=> __("Options Saved", 'acf'),
				
	);

	acf_add_options_page($args);
	
}

function anke_landing_room(){

	$args = array(
		'post_type' => 'room',
		'posts_per_page' => 1,		
	);

	$loop = new WP_Query( $args );

	ob_start();

	if ( $loop->have_posts() ) :		
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="landing-room-wrapper">
				<div class="thumb-wrap">
					<a href="<?php echo home_url('/rooms')?>">
						<?php the_post_thumbnail('full'); ?>
					</a>
				</div>
				<h1><?php the_title(); ?></h1>
				<h4><?php the_field('subtitle'); ?></h4>
					
			</div><?php 
		endwhile;
	endif;

	wp_reset_query();

	echo ob_get_clean();
}




/*Admin CSS*/
function sandhas_admin_css(){
    echo '<style>
    .wc-shipping-zone-settings tbody {
        display: table-row-group;
    }
    </style>';
}
add_action('admin_head','sandhas_admin_css');