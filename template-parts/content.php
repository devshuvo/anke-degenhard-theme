<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Sandhas
 */

$classes = array();

$classes[] = 'col-md-6';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

	<?php sandhas_post_thumbnail(); ?>


</article><!-- #post-<?php the_ID(); ?> -->
